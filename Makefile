run: eddcheck.img sample0.qcow2 sample1.qcow2 sample2.qcow2 sample3.qcow2
	qemu-system-i386 \
		-hda sample0.qcow2 \
		-hdb sample1.qcow2 \
		-hdc sample2.qcow2 \
		-hdd sample3.qcow2 \
		-drive if=floppy,format=raw,file=eddcheck.img

eddcheck.bin: eddcheck.s
	nasm -f bin -o eddcheck.bin eddcheck.s

eddcheck.img: eddcheck.bin
	cp eddcheck.bin eddcheck.img
	dd if=/dev/zero of=eddcheck.img bs=512 seek=1 count=2879

sample0.qcow2:
	qemu-img create -f qcow2 -o cluster_size=2M \
		sample0.qcow2 268435456

sample1.qcow2:
	qemu-img create -f qcow2 -o cluster_size=2M \
		sample1.qcow2 4294967296

sample2.qcow2:
	qemu-img create -f qcow2 -o cluster_size=2M \
		sample2.qcow2 1099511627776

sample3.qcow2:
	qemu-img create -f qcow2 -o cluster_size=2M \
		sample3.qcow2 4398046511104

clean:
	rm -f eddcheck.bin eddcheck.img sample?.qcow2
