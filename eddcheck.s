; eddcheck is a real mode MBR diagnostic tool for BIOS disk services.

; This program requires a PC-like computer that has an 80386 or newer.

bits 16
org 0x7C00

head:

text_head:

	; Some computers jump to CS:IP = 0x0000:0x7C00 to complete the
	; boot process, while others instead jump to 0x07C0:0x0000.
	; Which segment:address pair is used doesn't actually matter
	; for this program, because the only jumps and calls used are
	; relative to IP. Therefore, a far jump is not needed.

	jmp start

; puts(in AL, clobber DF = 0)
; Print the C-style string at [data_head + AL].

_puts:

	cld
	pusha

	; AX <- actual address of the string at location AL
	xor ah,ah
	add ax,data_head

	; LODS uses SI
	xchg si,ax

	; BX <- INT 0x10: page number | unused
	xor bx,bx

	.loop:
		lodsb

		; Check AL for a NUL terminator
		cmp al,0

		; Do we have more to print?
		je .loop_out

		; INT 0x10 (in AH = 0x0E, AL,
		;           BH = 0, BL = 0, clobber BP)
		; AH <- function code: teletype output
		; AL <- character to write
		; BH <- page number
		; BL <- unused in text mode
		; BP -> may be clobbered by a buggy routine
		mov ah,0x0E
		;;; mov al,...
		;;; xor bx,bx
		push bp
		int 0x10
		pop bp

		jmp .loop
	.loop_out:

	popa

ret

; puts?
; 3 bytes, in AL, clobber DF = 0
; Print the string whose location is in AL.

%macro puts? 0
	call _puts
%endmacro

; puts? address_expression
; 5 bytes, clobber AL, DF = 0
; Print the string at the given address.

%macro puts? 1
	puts?set %1
	puts?
%endmacro

; puts?n address_expression
; 7 bytes, clobber DF = 0
; Print the string at the given address, without clobbering AL.

%macro puts?n 1
	push ax
	puts? %1
	pop ax
%endmacro

; puts?set address_expression
; 2 bytes, out AL
; Make subsequent puts() calls use the given address.

%macro puts?set 1
	mov al,(%1 - data_head)
%endmacro

; putir(in EAX, BP, clobber DF = 0)
; Print the uint32 in EAX with a radix of BP.

_putir:

	std
	pusha
	push eax
	push ecx
	push edx

	; BX: XLAT: base address for the digits vector
	mov bx,putir.digits

	; DI: next address to be filled
	mov di,(putir.buffer_tail - 2)

	; ECX now owns the input value
	mov ecx,eax

	.loop:
		; EDX: DIV: headroom
		xor edx,edx

		; EAX: DIV: divisor
		movzx eax,bp

		; EAX borrows the input value
		; ECX now has the divisor
		xchg eax,ecx

		; EAX: EDX:EAX / ECX
		; EDX: EDX:EAX % ECX
		div ecx

		; ECX: updated input value
		xchg ecx,eax

		; AX: index into the digits vector
		xchg ax,dx

		; AL: printable digit
		xlatb

		; Store digit in buffer_putn
		stosb

	; Do we have any more output?
	; JECXZ + JMP = 5 bytes
	; INC + LOOP = 4 bytes
	inc ecx
	loop .loop

	; The last stosb moved DI too
	inc di

	; Adjust the string address: TODO comment
	xchg ax,di
	sub ax,data_head

	; Time to print!
	puts?

	pop edx
	pop ecx
	pop eax
	popa

ret

; putir?
; 3 bytes, in EAX, BP, clobber DF = 0
; Print the uint32 in EAX with a radix of BP.

%macro putir? 0
	call _putir
%endmacro

; putir? r32/m32/imm32/moffs32
; 6/6/9/9 bytes, in BP, clobber EAX, DF = 0
; Print the given uint32 with a radix of BP.

%macro putir? 1
	mov eax,%1
	putir?
%endmacro

; putir?s r8/m8/r16/m16
; 7 bytes, in BP, clobber EAX, DF = 0
; Print the given uint8/8/16/16 with a radix of BP.

%macro putir?s 1
	movzx eax,%1
	putir?
%endmacro

; putir?sh r8/m8/r16/m16
; 12 bytes, clobber EAX, DF = 0
; Print the given uint8/8/16/16 in hexadecimal.

%macro putir?sh 1
	push bp
	mov bp,16
	putir?s %1
	pop bp
%endmacro

start:

; eddcheck()
; Configure the machine that has just been booted, look for fixed
; storage devices, then gather and print some details about them.
; Assumes few, clobbers many, and should never return.

_eddcheck:

	; Sane defaults
	xor ax,ax
	mov ss,ax
	mov ds,ax
	mov es,ax

	; puts() or putir() are called at least once before we ever
	; use a string operation in eddcheck(), so cld may be omitted
	;;; cld

	; Place the stack just below this program
	mov sp,head

	; BP <- global variable for putir(): radix
	mov bp,10

	; INT 0x10 (in AH = 0x00, AL = 0, clobber AL)
	; AH <- function code: set video mode
	; AL <- CGA mode 2: 0xB8000, 80 by 25 text, no colour burst
	; AL -> various CRTC or video mode information
	add al,2
	int 0x10

	puts? eddcheck.title

	; Start just before the first fixed device
	; DL <- device number for INT 0x13 routines
	mov dl,0x7F

.device:

	; Next device!
	inc dx
	push dx
	push dx

	; INT 0x13 (in AH = 0x08, DL, ES = 0x0000, DI = 0x0000,
	;           out CF, CL:CH, DH, clobber AH, AL, BL, DL)
	; AH     <- function code: query legacy device parameters
	; DL     <- device number
	; ES, DI <- set both to 0x0000 to avoid some BIOS bugs
	; CF     -> routine failed?
	; CL:CH  -> bits mixed together like [ccssssss|cccccccc] where
	;            * [cc...cccccccc] (in that order) is the highest
	;              logical cylinder number (zero based); and
	;            * [ssssss] is the highest logical sector number
	;              in any given track (one based)
	; DH     -> highest logical head number (zero based)
	; AH     -> status/error code (should be 0x00 when CF = 0)
	; AL     -> unknown meaning (may be clobbered by the routine)
	; BL     -> device type code (not useful for fixed devices)
	; DL     -> number of devices of the same kind as the subject
	mov ah,0x08
	;;; mov dl,...
	;;; xor cx,cx
	;;; mov es,cx
	;;; xor di,di
	int 0x13

	; Was there an error?
	jc .finish

	; -> device 80h:
	pop bx
	puts? eddcheck.device
	putir?sh bl
	puts? eddcheck.hcs
	push bx

	; -> 1024,
	movzx eax,cx
	xchg al,ah
	shr ah,6
	inc ax
	putir?
	puts? eddcheck.comma

	; -> 256,
	movzx ax,dh
	inc ax
	putir?
	puts? eddcheck.comma

	; Only the final value can do without a movzx

	; -> 63
	xchg ax,cx
	and ax,0x003F
	putir?

	; -> 41
	puts? eddcheck.extensions

	; INT 0x13 (in AH = 0x41, BX = 0x55AA, DL,
	;           out CF, AH, CX, clobber AL, BX = 0xAA55, DH)
	; AH     <- function code: query for extension routines
	; BX     <- magic signature
	; DL     <- device number
	; CF     -> routine failed?
	; AH     -> major version of extensions if CF = 0, or
	;           status/error code if CF = 1
	; CX     -> interface subset support bitmap
	; AL     -> unknown meaning (may be clobbered by the routine)
	; BX     -> magic signature (should be checked like CF)
	; DH     -> unknown meaning (may be clobbered by the routine)
	mov ah,0x41
	mov bx,[signature]
	xchg bl,bh
	pop dx
	int 0x13
	push ax

	; Are any extensions supported?
	jnc .extensions_installed
		; -> h: error 13
		puts? eddcheck.hcs
		pop ax
		jmp .error
	.extensions_installed:
		; -> h/30
		puts? eddcheck.hsla
		pop ax
		putir?sh ah

		; Do we have access to the EDD subset?
		test cl,0x04
		jnz .edd_subset
			; -> h:
			puts? eddcheck.hcs
			jmp .edd_subset_out
		.edd_subset:
			; -> h/EDD:
			puts? eddcheck.edd
		.edd_subset_out:

	; INT 0x13 (in AH = 0x48, DL, DS, SI, [SI], out CF, AH, [SI]+)
	; AH     <- function code: query extended device parameters
	; DL     <- device number
	; DS:SI  <- address of the result buffer to be written to
	; [SI]   <- word: maximum length of the result buffer
	; CF     -> routine failed?
	; AH     -> status/error code (should be 0x00 when CF = 0)
	; [SI]+  -> the result buffer
		mov ah,0x48
		;; mov dl,...
		;; xor cx,cx
		;; mov ds,cx
		mov si,0x7E00
		mov word [si],0x200
		int 0x13

		; Was there an error?
		jc .error

		; word SI[0] and SI[2] are not used by us
		lodsd

		; dword SI[4] -> total physical cylinders
		lodsd

		; -> 16383,
		putir?
		puts? eddcheck.comma

		; dword SI[8] -> total physical heads
		lodsd

		; -> 16,
		putir?
		puts? eddcheck.comma

		; dword SI[12] -> total physical sectors/track
		lodsd

		; -> 63
		putir?
		puts? eddcheck.ending

		; dword SI[16] -> total physical sectors, lower
		lodsd
		xchg ebx,eax

		; dword SI[20] -> total physical sectors, upper
		lodsd
		xchg edx,eax

		; word SI[24] -> bytes in each physical sector
		lodsw
		movzx eax,ax

		; -> 512 bytes/sector
		putir?
		puts?n eddcheck.each

		; EBX <- bytes in each physical sector
		; EAX <- total physical sectors, lower
		; EDX <- total physical sectors, upper
		xchg ebx,eax

		; Do we have too many sectors for 32 bits?
		test edx,edx
		jnz .more_sectors
			; -> 15482880 sectors
			putir?
			puts? eddcheck.total

			; EDX:EAX <- total capacity in bytes
			; Guaranteed not to overflow:
			; 2^32 sectors * 2^16 B = 2^48 < 2^64 B
			mul ebx

			; EAX: total capacity in MiB
			; Guaranteed not to throw #DE:
			; 2^48 B / 2^20 = 2^28 < 2^32 MiB
			mov cl,20
			shrd eax,edx,cl

			; -> 7560 MiB
			putir?
			puts?set eddcheck.capacity
			jmp .more_sectors_out
		.more_sectors:
			puts?set eddcheck.more
		.more_sectors_out:

		; Print eddcheck.capacity or eddcheck.more
		puts?
	.extensions_installed_out:

	; falls through

.device_out:

.next:

	puts? eddcheck.ending
	pop dx
	jmp .device

.next_out:

.error:

	; -> error 13
	puts? eddcheck.error
	putir?s ah
	jmp .next

.error_out:

.finish:

	; ASSUMPTION: INT 0x13 device numbers are consecutive
	cli
	hlt

.finish_out:

text_tail:

times (512 + text_head - text_tail + data_head - data_tail) nop

data_head:

eddcheck:
	.comma:         db ', ',0
	.hcs:           db 'h: ',0                ; fall through
	.hsla:          db 'h/',0
	.error:         db 'error ',0
	.device:        db 13,10,'device ',0
	.extensions:    db 13,10,'41',0
	.edd:           db 'h/EDD: ',0
	.each:          db ' bytes/sector',13,10,0
	.total:         db ' sectors',13,10,0
	.capacity:      db ' MiB',0
	.more:          db 'more than 2^32 sectors',0
	.title:         db 3,' eddcheck ',3       ; falls through
	.ending:        db 13,10,0

putir:
	.buffer_head:
	                db '..........',0
	.buffer_tail:
	.digits:        db '0123456789ABCDEF'

signature:              dw 0xAA55

data_tail:

tail:
